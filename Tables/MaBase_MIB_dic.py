##############################
##  Les bases de travail du répertoire ./MIB_Files/
##############################


BaseCabines =  [{'NoCabine': '1', 'NoAllee': '1'}, {'NoCabine': '2', 'NoAllee': '1'}, {'NoCabine': '3', 'NoAllee': '1'}, {'NoCabine': '4', 'NoAllee': '1'}, {'NoCabine': '5', 'NoAllee': '1'}, {'NoCabine': '6', 'NoAllee': '2'}, {'NoCabine': '7', 'NoAllee': '2'}, {'NoCabine': '8', 'NoAllee': '2'}, {'NoCabine': '9', 'NoAllee': '2'}]

BaseGardiens =  [{'NomAgent': 'Branno', 'NoCabine': '1'}, {'NomAgent': 'Darell', 'NoCabine': '2'}, {'NomAgent': 'Demerzel', 'NoCabine': '3'}, {'NomAgent': 'Seldon', 'NoCabine': '4'}, {'NomAgent': 'Dornick', 'NoCabine': '5'}, {'NomAgent': 'Hardin', 'NoCabine': '6'}, {'NomAgent': 'Trevize', 'NoCabine': '7'}, {'NomAgent': 'Pelorat', 'NoCabine': '8'}, {'NomAgent': 'Riose', 'NoCabine': '9'}]

BaseResponsables =  [{'NoAllee': '1', 'NomAgent': 'Seldon'}, {'NoAllee': '2', 'NomAgent': 'Pelorat'}]

BaseMiams =  [{'NomAlien': 'Zorglub', 'Aliment': 'Bortsch'}, {'NomAlien': 'Blorx', 'Aliment': 'Bortsch'}, {'NomAlien': 'Urxiz', 'Aliment': 'Zoumise'}, {'NomAlien': 'Zbleurdite', 'Aliment': 'Bortsch'}, {'NomAlien': 'Darneurane', 'Aliment': 'Schwanstucke'}, {'NomAlien': 'Mulzo', 'Aliment': 'Kashpir'}, {'NomAlien': 'Zzzzzz', 'Aliment': 'Kashpir'}, {'NomAlien': 'Arghh', 'Aliment': 'Zoumise'}, {'NomAlien': 'Joranum', 'Aliment': 'Bortsch'}]

BaseVilles =  [{'Ville': 'Terminus', 'Planete': 'Trantor'}, {'Ville': 'Arcturus', 'Planete': 'Euterpe'}, {'Ville': 'Kalgan', 'Planete': 'Helicon'}, {'Ville': 'Hesperos', 'Planete': 'Euterpe'}, {'Ville': 'Siwenna', 'Planete': 'Gaia'}]

BaseAliens =  [{'NomAlien': 'Zorglub', 'Sexe': 'M', 'Planete': 'Trantor', 'NoCabine': '1'}, {'NomAlien': 'Blorx', 'Sexe': 'M', 'Planete': 'Euterpe', 'NoCabine': '2'}, {'NomAlien': 'Urxiz', 'Sexe': 'M', 'Planete': 'Aurora', 'NoCabine': '3'}, {'NomAlien': 'Zbleurdite', 'Sexe': 'F', 'Planete': 'Trantor', 'NoCabine': '4'}, {'NomAlien': 'Darneurane', 'Sexe': 'M', 'Planete': 'Trantor', 'NoCabine': '5'}, {'NomAlien': 'Mulzo', 'Sexe': 'M', 'Planete': 'Helicon', 'NoCabine': '6'}, {'NomAlien': 'Zzzzzz', 'Sexe': 'F', 'Planete': 'Aurora', 'NoCabine': '7'}, {'NomAlien': 'Arghh', 'Sexe': 'M', 'Planete': 'Nexon', 'NoCabine': '8'}, {'NomAlien': 'Joranum', 'Sexe': 'F', 'Planete': 'Euterpe', 'NoCabine': '9'}]

BaseAgents =  [{'NomAgent': 'Branno', 'VilleAgent': 'Terminus'}, {'NomAgent': 'Darell', 'VilleAgent': 'Terminus'}, {'NomAgent': 'Demerzel', 'VilleAgent': 'Arcturus'}, {'NomAgent': 'Seldon', 'VilleAgent': 'Terminus'}, {'NomAgent': 'Dornick', 'VilleAgent': 'Kalgan'}, {'NomAgent': 'Hardin', 'VilleAgent': 'Terminus'}, {'NomAgent': 'Trevize', 'VilleAgent': 'Hesperos'}, {'NomAgent': 'Pelorat', 'VilleAgent': 'Kalgan'}, {'NomAgent': 'Riose', 'VilleAgent': 'Terminus'}, {'NomAgent': 'Palver', 'VilleAgent': 'Siwenna'}, {'NomAgent': 'Amaryl', 'VilleAgent': 'Arcturus'}]

