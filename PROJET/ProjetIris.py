from copy import deepcopy
import csv
from math import sqrt

def depuis_csv(nom_fichier_csv):
    """
    Crée une liste de dictionnaires, un par ligne.
    La 1ère ligne du fichier csv est considérée comme la ligne des noms des champs.
    
    """
    lecteur = csv.DictReader(open(nom_fichier_csv,'r'))
    return [dict(ligne) for ligne in lecteur]


def vers_csv(nom_table, ordre_cols):
    """
    Exporte une liste de dictionnaires représentant une table sous forme d'un
    fichier csv. On rentre le nom de la table sous forme de chaîne.
    On donne l'ordre des colonnes sous la forme d'une liste d'attributs.

    >>> vers_csv('Groupe1', ordre_cols=['Nom','Anglais','Info','Maths'])
    """
    with open(nom_table + '.csv', 'w') as fic_csv:
        ecrit = csv.DictWriter(fic_csv, fieldnames=ordre_cols)
        table = eval(nom_table)
        ecrit.writeheader()
        for dic in table:
            ecrit.writerow(dic)
    return None


app = depuis_csv('Base_Apprentissage.csv')

test = depuis_csv('Base_Test.csv')
iris = depuis_csv('Base_Iris.csv')

def dm(x,y):
    return sum(abs(eval(x[i])-eval(y[i])) for i in ['x1','x2','x3','x4'])

def de(x,y):
    return sqrt(sum((eval(x[i])-eval(y[i]))**2 for i in ['x1','x2','x3','x4']))

def ppv(j,base,d,k):
    s = []
    x = base[j]
    for i in range(len(base)):
        if i != j:
            s.append((i,d(x,base[i])))
    sc = sorted(s, key=lambda dist: dist[1] )[:k]
    cl = [base[y[0]]['Classe'] for y in sc]
    return max(set(cl), key=cl.count)

def compare(base):
    n_b = deepcopy(base)
    ec = [0]*10
    n = len(base)
    for i in range(n):
        x = n_b[i]
        for k in range(1,6):
            a, b = 1 - int(ppv(i,base,dm,k) == x['Classe']), 1 - int(ppv(i,base,de,k) == x['Classe'])
            x[str(k) + '_man'] = a
            x[str(k) + '_euc'] = b
            ec[2*(k-1)] += a
            ec[2*(k-1) + 1] += b
    return n_b, [s/n for s in ec]

