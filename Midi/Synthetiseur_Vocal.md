# Synthétisuer vocal

Vous pouvez faire lire votre texte à votre machine avec `esound` et `mbrola`.

Installez ces deux utilitaires:

```shell
$ sudo apt-get install esound, mbrola
```

Récupérez des voix françaises moins robotiques:

```shell
$ sudo mkdir /usr/share/mbrola/
$ cd /usr/share/mbrola
$ sudo wget https://github.com/numediart/MBROLA-voices/raw/master/data/fr1/fr1
$ sudo wget https://github.com/numediart/MBROLA-voices/raw/master/data/fr4/fr4
$ sudo wget https://github.com/numediart/MBROLA-voices/raw/master/data/en1/en1
```


et laissez-vous bercer:

```shell
$ espeak -v mb-en1 -s 140 "I can't get no satisfaction, but I try"

$ espeak -v mb-fr4 -s 120 'Hâtez-vous lentement, et, sans perdre courage, Vingt fois sur le métier, remettez votre ouvrage. Polissez-le sans cesse et le repolissez ; Ajoutez quelquefois, et souvent effacez. '
```


et pour avoir la date et l'heure:

```shell
$ date "+Nous sommes le  %A %d %B %Y. Il est %H heures et %M minutes. Que la force soit avec vous." | espeak -v mb-fr4 -s 120
```
