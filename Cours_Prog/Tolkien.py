"""
Crée trois classes de personnages pour illustrer très simplement la notion de classes et d'héritage  
"""

from dataclasses import dataclass


@dataclass
class Etre:
    """
    Un être a trois attributs et deux méthodes
    """
    appellation: str = ''
    poids      : int = 0
    bonjour    : str = ''

    def salut(self) -> None:
        """
        Salut des êtres, quelque soit leur langue
        """
        print("%s ! Mon nom est %s" % (self.bonjour, self.appellation))

    def masse(self) -> None:
        """
        Affiche la masse de l'être
        """
        print("Je pèse %d kg" % self.poids)


@dataclass
class Orque(Etre):
    """
    Un Orque est un être particulier ayant son propre bonjour et un maître
    """
    bonjour: int = 'Ash nazg durbatulûk'
    maitre : str = 'Sauron'

    
class Hobbit(Etre):
    """
    Un Hobbit a une appellation constituée de deux attributs propres
    """

    def __init__(self, nom: str, prenom: str, poids: int):
        """
        Un Hobbit a un prénom et un nom qui constituent son appellation d'Être
        """
        Etre.__init__(self, prenom + ' ' + nom, poids, "Bonjour")
        self.nom   : str = nom
        self.prenom: str = prenom
