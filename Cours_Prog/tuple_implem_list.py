from typing import Tuple, TypeVar, Any

T = TypeVar('T')
Liste = Tuple[T, Any]

Vide:Liste = (None, None)

def liste( x:T, xs:Liste[T] ) -> Liste[T] :
    if x is None:
        return Vide
    return (x,xs)

def est_vide( xs:Liste[T] ) -> bool :
    return xs == (None,None)

def tete( xs:Liste[T] ) -> T :
    assert not(est_vide(xs)), 'Liste vide'
    return xs[0]

def queue( xs:Liste[T] ) -> Liste[T] :
    assert not(est_vide(xs)), 'Liste vide'
    return xs[1]

def insere(x:T, xs:Liste[T]) -> Liste[T]:
    return (x, xs)



##########################
#
# Barrière d'abstraction
#
############################

xs:Liste[int] = Vide

xs = insere(2, xs)

xs = insere('q', xs)

def long(xs:Liste[T]) -> int:
    if est_vide(xs):
        return 0
    else:
        return 1 + long(queue(xs))
