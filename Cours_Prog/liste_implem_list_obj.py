from copy import copy
from typing import TypeVar, Generic, Optional

T = TypeVar('T')

class Liste(Generic[T]):

    def __init__(self):
        "docstring"
        self.tete:T = None

    def insere(self, new_tete: T) -> None:
        if self.est_vide():
            self.tete = new_tete
            self.queue:Liste[T] = Liste()
        else:
            self.queue:Liste[T] = copy(self)
            self.tete = new_tete

    def est_vide(self) -> bool:
        return self.tete is None

    def __repr__(self) -> str:
        if self.est_vide():
            return 'Liste_Vide'
        else:
            return str(self.tete) + ':(' + self.queue.__repr__() + ')'

# Définitions communes 
        

def liste(x:T, xs: Liste[T]) -> Liste[T]:
    xs.insere(x)
    return xs

def est_vide(xs:Liste[T]) -> bool:
    return xs.est_vide()

def tete(xs:Liste[T]) -> T:
    assert not(est_vide(xs)), 'Liste vide'
    return xs.tete

def queue(xs:Liste[T]) -> Liste[T]:
    assert not(est_vide(xs)), 'Liste vide'
    return xs.queue

def insere(x: T, xs: Liste[T]) -> Liste[T]:
    xs.insere(x)
    return xs

Vide:Liste = Liste()


##########################
#
# Barrière d'abstraction
#
############################


xs:Liste[int] = Vide

xs = insere(2, xs)

xs = insere('deux', xs)

def long(xs:Liste[T]) -> int:
    if est_vide(xs):
        return 0
    else:
        return 1 + long(queue(xs))
