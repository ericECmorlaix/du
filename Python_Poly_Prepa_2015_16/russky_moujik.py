def russky_moujik(x,y) :
    def moujikchou(a,b,acc) :
        if a == 0 : 
            return acc
        return moujikchou(a//2, b*2, acc + b*(a % 2))
    return moujikchou(x, y, 0)
    
def russian_peasant(x,y) :
    a, b, acc = x, y, 0
    while a > 0 :
        acc += b*(a % 2)
        b *= 2
        a //= 2
        print(a*b + acc)
    return acc
    
        