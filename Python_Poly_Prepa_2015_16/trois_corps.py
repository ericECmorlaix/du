import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

G = 1
m = 1
position = np.array([[0.97000436,-0.24308753],[-0.97000436,0.24308753],[0,0]])
position2 = np.array([[1.07000436,-0.24308753],[-0.97000436,0.24308753],[0,0]])
vitesse = np.array([[ 0.46620368,  0.43236573],[ 0.46620368,  0.43236573],[-0.93240737,-0.86473146]])

def norme(v) :
    """ norme d'un vecteur"""
    return (v[0]**2 + v[1]**2)**0.5

def force2(m1,p1, m2, p2) :
    """ force exercée par le corps 2 sur le corps 1"""
    p1p2 =  p2 - p1
    return ((G*m1*m2)/norme(p1p2)**3) * p1p2

def force(j,m,pos) :
    """ somme des forces exercées sur le corps j
        on pourra utiliser np.sum(array,axis=0)"""
    return np.sum(np.array([force2(m, pos[j], m, pos[k]) for k in range(3) if k != j]),axis=0)

def gravit(pos) :
    """renvoie le vecteur dont les composantes sont les accélérations de chaque corps pour une position donnée """
    return np.array([force(j,m,pos) for j in range(3) ])

def verlet(f, pos0, vit0, h, n) :
    """ on utilise un schéma de Verlet vectoriel"""
    pos, vit = pos0, vit0
    poss = [pos]
    for i in range(n - 1) :
        fpos     = f(pos)
        pos      = pos + h*vit + 0.5*h**2*fpos
        fpos_new = f(pos)
        vit      = vit + 0.5*h*(fpos + fpos_new)
        poss.append(pos)
    return [[poss[tps][pt,coord] for tps in range (len(poss))] for coord in range(2) for pt in range(3)]

def phase(f, pos0, vit0, h, n, no) :
    """ trace les mouvements des trois corps"""
    data = verlet(f, pos0, vit0, h, n)
    for p in range(3) :
        plt.plot(data[p], data[p+3])
    plt.savefig('TroisCorps' + str(no) + '.pdf')


# fig = plt.figure() # initialise la figure
# ax = plt.axes(xlim=(-2, 2), ylim=(-2, 2))
# #line, = ax.plot([],[]) 
# Writer = animation.writers['ffmpeg']
# writer = Writer(fps=100, metadata=dict(artist='Me'), bitrate=1800)

# data = verlet(gravit, position, vitesse, 2**-5, 20*2**5)
# don = np.array([data[0],data[3]])
# don2 = np.array([data[1],data[4]])
# don3 = np.array([data[2],data[5]])

# def init():
#     line.set_data([], [],'-r')
#     return line,

# line, = plt.plot([], [],'-r')

# def animate(i,dat):
#     line.set_data(dat[..., :i])
#     return line,

# ani = animation.FuncAnimation(fig, animate, fargs=[don],frames=20*32,interval=100, blit=True)
# ani = animation.FuncAnimation(fig, animate, fargs=[don2],frames=20*32,interval=100, blit=True)

# ani.save('Trois.mp4', writer = writer)
